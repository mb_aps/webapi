﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin_API.Models
{
    public class gallerymodel
    {

        public int ImageID { get; set; }
        public string ImageName { get; set; }
        public string ImageUrl { get; set; }
        public Nullable<int> GalleryId { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
    }
}