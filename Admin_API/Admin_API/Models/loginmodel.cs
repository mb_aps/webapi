﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin_API.Models
{
    public class loginmodel
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public Nullable<int> UserType { get; set; }
    }
}