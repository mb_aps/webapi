﻿using Admin_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Admin_API.Controllers
{
    public class galleryController : Controller
    {
        //
        // GET: /gallery/
        private angularEntities2 ang = new angularEntities2();
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult SelectAll()
        {

            try
            {
                var result = from f in ang.galleries
                             select new
                             {
                                 f.ImageID,
                                 f.ImageName,
                                 f.ImageUrl,
                                 f.GalleryId,
                                 f.CreatedOn,
                                 f.UpdatedOn,
                             };
                if (result.ToList().Count > 0)
                {
                    return Json(new { ResultCode = 1, gal = result }, JsonRequestBehavior.AllowGet);
                }
                else
                {

                    return Json(new { ResultCode = 0, msg = "No Data Available" }, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                return Json(new { resultCode = 0, msg = ex });
            }
        }
        [HttpGet]
        public ActionResult SelectById(int galID)
        {
            try
            {

                var result = from f in ang.galleries
                             where f.ImageID.Equals(galID)
                             select new
                             {

                                 f.ImageID,
                                 f.ImageName,
                                 f.ImageUrl,
                                 f.GalleryId,
                                 f.CreatedOn,
                                 f.UpdatedOn,

                             };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { resultCode = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]

        public ActionResult DeleteById(int galID)

        {

            try
            {
                var deleteGallery = ang.galleries.Find(galID);
                ang.galleries.Remove(deleteGallery);
                int i = ang.SaveChanges();

                return Json(new { resultCode = i }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { resultCode = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult EditById(gallerymodel gm)
        {
            using (angularEntities2 asn = new angularEntities2())
            {
                var result = (from fr in asn.galleries
                              where fr.ImageID.Equals(gm.ImageID)
                              select fr).FirstOrDefault();
                if (result != null)
                {
                    result.ImageName = gm.ImageName;
                    result.ImageUrl = gm.ImageUrl;
                    result.CreatedOn = gm.CreatedOn;
                    result.UpdatedOn = gm.UpdatedOn;
                    int i = asn.SaveChanges();
                    if (i > 0)
                    {

                        return Json(new { resultCode = 1, msg = "sucess" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { resultCode = 0, msg = "failed" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { resultCode = 0, msg = "No Record Found" }, JsonRequestBehavior.AllowGet);
                }
               
            }

        }
    }
}