﻿using Admin_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Admin_API.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Login/
        public ActionResult Index()
        {
            return View();
        }


        private angularEntities2 ang = new angularEntities2();

        [HttpGet]
        public ActionResult LoginUser(string UserName, string Password)
        {
            try
            {
                var result = from f in ang.logins
                             where f.UserName.Equals(UserName) && f.Password.Equals(Password)
                             select new
                             {


                             };

                if (result.ToList().Count > 0)
                {
                    return Json(new { ResultCode = 1, log = result, msg = "Login Sucess" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { ResultCode = 0, msg = "No Data Available" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { ResultCode = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }


        }
        [HttpPost]
        public ActionResult ChangePassword(string UserName, string OldPassword, string NewPassword, string ConfrimPassword)
        {

            try
            {
                var result = (from f in this.ang.logins
                              where f.UserName.Equals(UserName)

                              where f.Password.Equals(OldPassword)

                              select f).FirstOrDefault();

                if (result != null)
                {

                    if (NewPassword == ConfrimPassword)
                    {
                        result.Password = NewPassword;
                        ang.SaveChanges();

                        return Json(new { ResultCode = 1, log = result, msg = "Password has been changed successfully" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { ResultCode = 0, log = result, msg = "Confirm Password and New Password is not matching" }, JsonRequestBehavior.AllowGet);

                    }
                }
                else
                {

                    return Json(new { ResultCode = 0, log = result, msg = "UserId or Password does not Exits" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { ResultCode = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }
    }
}