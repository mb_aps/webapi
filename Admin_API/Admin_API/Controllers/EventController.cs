﻿using Admin_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Admin_API.Controllers
{
    public class EventController : Controller
    {
        //
        // GET: /Event/
        private angularEntities2 ang = new angularEntities2();
       
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult SelectAllEvents()
        {
            try
            {
                var result = from evn in ang.events
                             select new
                             {
                                evn.EventId,
                                evn.EventTitle,
                                evn.EventDescription,
                                evn.CreatedOn,
                                evn.UpdatedOn,
                             };
                if (result.ToList().Count > 0)
                {
                    return Json(new { ResultCode = 1, gal = result }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { ResultCode = 0, msg = "No Data Available" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { resultCode = 0, msg = ex }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult SelectEventsById(int id)
        {
            try
            {
                var result = from evn in ang.events
                             where evn.EventId.Equals(id)
                             select new
                             {
                                 evn.EventId,
                                 evn.EventTitle,
                                 evn.EventDescription,
                                 evn.CreatedOn,
                                 evn.UpdatedOn,
                             };
                return Json(result,JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { resultCode = 0, msg = ex.Message });
            }
        }

        [HttpPost]
        public ActionResult DeleteById(int EventId)
        {

            try
            {
                var result = ang.events.Find(EventId);
                ang.events.Remove(result);
                int i = ang.SaveChanges();
                return Json(new { resultCode = i },JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { resultCode = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        public ActionResult EditDetails(EventModel em)
        {
            using (angularEntities2 asn = new angularEntities2())
            {
                var result = (from evn in asn.events
                              where evn.EventId.Equals(em.EventId)
                              select evn).FirstOrDefault();
                if (result != null)
                {
                    result.EventTitle = em.EventTitle;
                    result.EventDate = em.EventDate;
                    result.EventDescription = em.EventDescription;
                    int i = asn.SaveChanges();
                    if (i > 0)
                    {

                        return Json(new { ResultCode = 1, msg = "Sucess" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { ResultCode = 0, msg = "No Data Available" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { resultCode = 0, msg = "No Data Available" }, JsonRequestBehavior.AllowGet);
                }
            }
        }
	}
}