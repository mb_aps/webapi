﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Admin_API.Startup))]
namespace Admin_API
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
